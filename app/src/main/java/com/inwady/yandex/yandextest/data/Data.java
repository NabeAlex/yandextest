package com.inwady.yandex.yandextest.data;

public class Data {

    public interface MusicData {
        String ID = "id";
        String NAME = "name";
        String GENRES = "genres";
        String TRACKS = "tracks";
        String ALBUMS = "albums";
        String LINK = "link";
        String DESCRIPTION = "description";

        String COVER = "cover";

        public interface ImageData {
            String SMALL = "small";
            String BIG = "big";
        }
    }
}
