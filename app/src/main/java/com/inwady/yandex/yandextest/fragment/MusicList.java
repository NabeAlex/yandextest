package com.inwady.yandex.yandextest.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.app.Fragment;

import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.inwady.yandex.yandextest.MainActivity;
import com.inwady.yandex.yandextest.R;
import com.inwady.yandex.yandextest.model.App;
import com.inwady.yandex.yandextest.network.MusicRequest;
import com.inwady.yandex.yandextest.ui.adapter.MusicAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MusicList extends Fragment {
    RecyclerView.LayoutManager layoutManager = null;
    MusicAdapter musicAdapter = null;

    @Bind(R.id.list_music_view)
    RecyclerView recyclerView;

    @Bind(R.id.progress_music_list)
    ProgressBar progressBar;

    private View view = null;
    private boolean wasError = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view == null || wasError) {
            view = inflater.inflate(R.layout.fragment_music_list, container, false);

            ButterKnife.bind(this, view);

            recyclerView.setHasFixedSize(true); // But add image in list

            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);

            new MusicRequest(getResources().getString(R.string.download_music_list), new Handler() {
                public void handleMessage(Message msg) {
                    progressBar.setVisibility(View.GONE);

                    if(msg != null) {
                        Bundle bundle = msg.getData();

                        if(bundle.containsKey("ERROR")) {
                            ((MainActivity) getActivity()).generateError();
                            wasError = true;
                            return;
                        }
                    }

                    wasError = false;
                    musicAdapter = new MusicAdapter(getActivity(), App.musics);
                    recyclerView.setAdapter(musicAdapter);
                }
            }).sendRequest();
        }

        return view;
    }
}
