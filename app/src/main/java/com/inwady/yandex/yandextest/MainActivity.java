package com.inwady.yandex.yandextest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.inwady.yandex.yandextest.fragment.*;
import com.inwady.yandex.yandextest.model.InitFactory;
import com.inwady.yandex.yandextest.network.MusicRequest;

import org.json.JSONObject;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
        InitFactory.setBaseComponent(R.id.main);

        InitFactory.creater(InitFactory.TypeComponent.ACTIVITY).init(
                this,
                R.layout.activity_main,
                getResources().getString(R.string.fragment_music_list)
        );

        generateMusicList();
    }

    private void init() {
        InitFactory.addFragment(R.layout.fragment_music_list, new MusicList());
        InitFactory.addFragment(R.layout.fragment_musician, new Musician());
    }

    public void generateError() {
        setTitle("");
        getFragmentManager().beginTransaction().replace(R.id.main,
                new com.inwady.yandex.yandextest.fragment.Error()).commit();
    }

    public void generateMusicList() {
        InitFactory.creater(InitFactory.TypeComponent.FRAGMENT).init(
                this,
                R.layout.fragment_music_list,
                getResources().getString(R.string.fragment_music_list)
        );
    }

    @Override
    public void onBackPressed()
    {
        if(InitFactory.currentFragment == R.layout.fragment_music_list) {
            Log.d("!!", "!");

            super.onBackPressed();
        }
        if(InitFactory.currentFragment == R.layout.fragment_musician) {
            InitFactory.currentFragment = R.layout.fragment_music_list;

            InitFactory.creater(InitFactory.TypeComponent.FRAGMENT).close(this,
                    getResources().getString(R.string.fragment_music_list));

        }
    }

}
