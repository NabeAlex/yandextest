package com.inwady.yandex.yandextest.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inwady.yandex.yandextest.R;
import com.inwady.yandex.yandextest.data.Music;
import com.inwady.yandex.yandextest.model.App;
import com.inwady.yandex.yandextest.network.MusicRequest;
import com.inwady.yandex.yandextest.ui.adapter.MusicAdapter;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Musician extends Fragment {
    Music music = null;
    @Bind(R.id.image_musician)
    ImageView imageBig;

    @Bind(R.id.text_descr)
    TextView textDescription;

    @Bind(R.id.text_genres)
    TextView textGenres;

    @Bind(R.id.text_musics_count)
    TextView textCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_musician, container, false);

        ButterKnife.bind(this, view);
        music = App.stackCurrent.pop();

        Picasso.with(getActivity()).load(music.bigImage).fit().centerCrop().into(imageBig);

        String result = "";
        result += music.countAlbums + " альбомов";
        result += "  ·  ";
        result += music.countTracks + " песни";

        textCount.setText(result);
        textGenres.setText(music.toStringGenres());
        textDescription.setText(music.description);

        return view;
    }

}
