package com.inwady.yandex.yandextest.network;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.inwady.yandex.yandextest.data.Data;
import com.inwady.yandex.yandextest.data.Music;
import com.inwady.yandex.yandextest.model.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MusicRequest {
    private static final int DEFAULT_COUNT = 50;

    private static class ParseMusicJson {
        public static Music parseJson(JSONObject json) throws JSONException {
            Music tmp = null;
            try {
                tmp = new Music();

                tmp.id = json.getInt(Data.MusicData.ID);
                tmp.name = json.getString(Data.MusicData.NAME);
                Log.d("data", tmp.name);
                tmp.description = json.getString(Data.MusicData.DESCRIPTION);

                JSONArray jsonStyles = json.getJSONArray(Data.MusicData.GENRES);
                tmp.styles = new String[jsonStyles.length()];

                for (int i = 0 ; i < jsonStyles.length(); i++)
                    tmp.styles[i] = jsonStyles.getString(i);

                tmp.countAlbums = json.getInt(Data.MusicData.ALBUMS);
                tmp.countTracks = json.getInt(Data.MusicData.TRACKS);

                if(json.has(Data.MusicData.LINK))
                    tmp.userInfo = json.getString(Data.MusicData.LINK);

                JSONObject images = json.getJSONObject(Data.MusicData.COVER);
                tmp.smallImage = images.getString(Data.MusicData.ImageData.SMALL);
                tmp.bigImage = images.getString(Data.MusicData.ImageData.BIG);

            } catch (JSONException e) {
                throw e;
            }

            return tmp;
        }
    }

    private Handler handler = null;

    private String resource = null;
    public MusicRequest(String resource) {
        this.resource = resource;
    }

    public MusicRequest(String resource, Handler handler) {
        this.resource = resource;
        this.handler = handler;
    }

    public static ArrayList<Music> pack = null;

    public  void sendRequest() {
        StringRequest jsonRequest = new StringRequest(resource,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        JSONArray jsonArray = null;

                        try
                        {
                            jsonArray = new JSONArray(new String(response.getBytes("ISO-8859-1"), "UTF-8"));
                        }
                        catch (UnsupportedEncodingException e)
                        {
                            e.printStackTrace();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        pack = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                pack.add(ParseMusicJson.parseJson(jsonArray.getJSONObject(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        App.musics = pack;
                        if(handler != null) {
                            handler.handleMessage(null);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(handler != null) {
                            Message message = new Message();

                            Bundle bundle = new Bundle();
                            bundle.putString("ERROR", "");

                            message.setData(bundle);
                            handler.handleMessage(message);
                        }
                    }
        });

        App.getRequestQueue().add(jsonRequest);
    }

}
