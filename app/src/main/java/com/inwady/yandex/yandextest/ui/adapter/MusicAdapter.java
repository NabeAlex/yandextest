package com.inwady.yandex.yandextest.ui.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inwady.yandex.yandextest.R;
import com.inwady.yandex.yandextest.data.Music;
import com.inwady.yandex.yandextest.model.App;
import com.inwady.yandex.yandextest.model.InitFactory;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder> {
    private Activity act = null;

    private static ArrayList<Music> data;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Activity activity = null;

        public TextView name;
        public TextView genres;
        public TextView count;

        public ImageView image;

        public ViewHolder(View v, Activity act) {
            super(v);
            activity = act;

            v.setOnClickListener(this);

            name = (TextView) v.findViewById(R.id.text_musician_name);
            genres = (TextView) v.findViewById(R.id.text_genres);
            count = (TextView) v.findViewById(R.id.text_musics_count);

            image = (ImageView) v.findViewById(R.id.icon_musician);
        }

        public void setData(String name, String genres, String count) {
            this.name.setText(name);
            this.genres.setText(genres);
            this.count.setText(count);
        }

        @Override
        public void onClick(View v) {
            App.stackCurrent.add(data.get(getAdapterPosition()));
            InitFactory.creater(InitFactory.TypeComponent.FRAGMENT).init(activity, R.layout.fragment_musician,
                        this.name.getText().toString());
        }
    }

    public MusicAdapter(Activity act, ArrayList<Music> musics) {
        this.act = act;
        data = musics;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_musician, parent, false);

        ViewHolder vh = new ViewHolder(v, act);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Music music = data.get(position);

        /* ! */

        String count_musics = "";
        count_musics += music.countAlbums + " альбомов";
        count_musics += ", ";
        count_musics += music.countTracks + " песен";

        /*****/

        holder.setData(music.name, music.toStringGenres(), count_musics);

        Picasso.with(act).load(music.smallImage).fit().into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}