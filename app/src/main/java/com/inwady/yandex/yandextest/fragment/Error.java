package com.inwady.yandex.yandextest.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inwady.yandex.yandextest.MainActivity;
import com.inwady.yandex.yandextest.R;
import com.inwady.yandex.yandextest.data.Music;
import com.inwady.yandex.yandextest.model.App;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Error extends Fragment implements View.OnClickListener  {

    @Bind(R.id.button_reset)
    Button buttonReset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_error, container, false);

        ButterKnife.bind(this, view);

        buttonReset.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        ((MainActivity) getActivity()).generateMusicList();
    }
}