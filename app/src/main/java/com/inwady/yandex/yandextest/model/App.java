package com.inwady.yandex.yandextest.model;

import android.app.Application;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.inwady.yandex.yandextest.data.Music;

import java.util.ArrayList;
import java.util.Stack;

public class App extends Application {
    private static App app = null;

    public static Stack<Music> stackCurrent;

    static {
        stackCurrent = new Stack<>();
    }

    public static ArrayList<Music> musics = null;

    private static RequestQueue requestQueue = null;

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    @Override
    public void onCreate() {
        if (app != null)

            return;

        else
            app = this;

        super.onCreate();

        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());

        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();
    }

    public void cancelPendingReq(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

}
