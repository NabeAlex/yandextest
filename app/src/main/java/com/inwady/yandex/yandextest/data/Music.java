package com.inwady.yandex.yandextest.data;

public class Music {
    public int id;
    public String name;

    public String styles[];
    public String toStringGenres() {
        String result = "";
        for(int i = 0; i < styles.length; i++) {
            result += styles[i] + ((i != styles.length - 1) ? ", " : "");
        }
        return result;
    }

    public int countTracks;
    public int countAlbums;

    public String userInfo;
    public String description;

    public String smallImage;
    public String bigImage;


}
