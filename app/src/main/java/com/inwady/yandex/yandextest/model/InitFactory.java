package com.inwady.yandex.yandextest.model;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;

import com.inwady.yandex.yandextest.R;

import android.app.Fragment;
import android.os.Build;
import android.util.Log;

import java.util.HashMap;

public class InitFactory {
    public enum TypeComponent {
        ACTIVITY, FRAGMENT
    }

    public interface IInit {
        void init(Activity act, int layout, String title);
        void close(Activity act, String title);
    }

    public static class InitFragment implements IInit {

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        public void init(Activity act, int layout, String title) {
            if(title != null)
                act.setTitle(title);

            if(fm == null || fm.isDestroyed())
                fm = act.getFragmentManager();

            currentFragment = layout;
            if(fragmentMap.containsKey(layout)) {

                if(currentFragment == R.layout.fragment_error) {
                    Log.d("Wha", "th");
                    fm.beginTransaction().replace(baseComponent, fragmentMap.get(layout)).commit();
                    return;
                }

                fm.beginTransaction().replace(baseComponent, fragmentMap.get(layout)).addToBackStack("Music").commit();
            } else
                return;
        }

        public void close(Activity act, String title) {
            act.setTitle(title);
            act.getFragmentManager().popBackStack();
        }
    }

    public static class InitActivity implements IInit {

        public void init(Activity act, int layout, String title) {
            act.setContentView(layout);
            act.setTitle(title);
        }

        public void close(Activity act, String title) {
            act.finish();
        }
    }

    public static int currentFragment;

    public static FragmentManager fm;
    private static HashMap<Integer, Fragment> fragmentMap;
    private static int baseComponent;

    public static void setBaseComponent(int id) {
        baseComponent = id;
    }

    public static void addFragment(int id, Fragment fragment) {
        fragmentMap.put(id, fragment);
    }

    public static HashMap<Integer, Fragment> getFragmentMap() {
        return fragmentMap;
    }

    static {
        fm = null;
        baseComponent = 0x00;
        fragmentMap = new HashMap<>();
    }

    public static IInit creater(TypeComponent typeComponent) {
        if(typeComponent == TypeComponent.ACTIVITY)

            return new InitFactory.InitActivity();

        else if(typeComponent == TypeComponent.FRAGMENT)

            return new InitFactory.InitFragment();

        return null;
    }

}
